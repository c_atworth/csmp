// WebSocket Connection
const socket = new WebSocket('ws://change-me-to-exposed-websocket-host');

var connectedPlayerCounter = 0;
var disconnectedPlayerCounter = 0;

// DOM Elements
const connectedList = document.getElementById('connected-players');
const disconnectedList = document.getElementById('disconnected-players');
const connectedPlayerCount = document.getElementById('connected-player-count');
const disconnectedPlayerCount = document.getElementById('disconnected-player-count');

// WebSocket Listener
socket.addEventListener('message', (event) => {
    const data = JSON.parse(event.data);
    updateConnected(data);
    updateDisconnected(data);

});

// Connected Player List
function updateConnected(players) {
    connectedList.innerHTML = '';
    connectedPlayerCount.innerText = 'Getting Player Count...';
    connectedPlayerCounter = 0;

    const table = document.createElement('table');
    const tableHead = document.createElement('thead');
    const tableBody = document.createElement('tbody');

    const headerRow = document.createElement('tr');
    const nameHeader = document.createElement('th');
    const statusHeader = document.createElement('th');
    const healthHeader = document.createElement('th');
    const armorHeader = document.createElement('th');

    nameHeader.textContent = 'Name';
    statusHeader.textContent = 'Joined';
    healthHeader.textContent = 'Health';
    armorHeader.textContent = 'Armor';

    headerRow.appendChild(nameHeader);
    headerRow.appendChild(statusHeader);
    headerRow.appendChild(healthHeader);
    headerRow.appendChild(armorHeader);
    tableHead.appendChild(headerRow);

    for (let player of players) {
        if (player.online) {
            const row = document.createElement('tr');
            const nameCell = document.createElement('td');
            const statusCell = document.createElement('td');
            const healthCell = document.createElement('td');
            const armorCell = document.createElement('td');

            nameCell.textContent = player.name;
            statusCell.textContent = player.startTime ? `${new Date(player.startTime).toLocaleString()}` : '(Online status unavailable)';
            healthCell.textContent = player.health;
            armorCell.textContent = player.armor;

            row.appendChild(nameCell);
            row.appendChild(statusCell);
            row.appendChild(healthCell);
            row.appendChild(armorCell);
            tableBody.appendChild(row);

            connectedPlayerCounter++;
        }
    }

    table.appendChild(tableHead);
    table.appendChild(tableBody);
    connectedList.appendChild(table);
    connectedPlayerCount.innerText = `Connected Players: ${connectedPlayerCounter}`;
}

// Disconnected Player List
function updateDisconnected(players) {
    disconnectedList.innerHTML = '';
    disconnectedPlayerCount.innerText = 'Getting Disconnected Player Count...';
    disconnectedPlayerCounter = 0;

    const table = document.createElement('table');
    const tableHead = document.createElement('thead');
    const tableBody = document.createElement('tbody');

    const headerRow = document.createElement('tr');
    const nameHeader = document.createElement('th');
    const statusHeader = document.createElement('th');

    nameHeader.textContent = 'Name';
    statusHeader.textContent = 'Left';

    headerRow.appendChild(nameHeader);
    headerRow.appendChild(statusHeader);
    tableHead.appendChild(headerRow);

    for (let player of players) {
        if (!player.online) { 
            const row = document.createElement('tr');
            const nameCell = document.createElement('td');
            const statusCell = document.createElement('td');

            nameCell.textContent = player.name;
            statusCell.textContent = player.endTime ? `${new Date(player.endTime).toLocaleString()}` : '(Offline status unavailable)';

            row.appendChild(nameCell);
            row.appendChild(statusCell);
            tableBody.appendChild(row);

            disconnectedPlayerCounter++;
        }
    }

    table.appendChild(tableHead);
    table.appendChild(tableBody);
    disconnectedList.appendChild(table);
    disconnectedPlayerCount.innerText = `Disconnected Players: ${disconnectedPlayerCounter}`;
}