# Collin's squaremap Playerlist
_A simple way to retreive and log player connections and disconnections with a real-time web interface._

To use this, you'll need:
- an existing MongoDB database
- Node.js >= v20.0

### Setup
1. Download the code with "Download ZIP" or `git clone https://github.com/collins-corner/csmp`
2. Change into the directory of wherever you downloaded the code to
3. Modify the `.en.v` with the details of your setup. _(Note: the URL of the map server needs to point to `/tiles/players.json`, as this is the API endpoint we gather data from)_
4. Rename `.en.v` to `.env` (Remove the `.` between the "n" and "v")
5. Change the host of ./public/script.js to wherever the websocket host is exposed. For instance, this could be "ws://example.com:3000"

Assuming you've set everything up correctly, you should be able to run `node app.js` and be off! If you see `Server started!` in your console, go to http://{server-ip}:{server-port} (replace with the values you defined in `.env`) in your browser, and you should see a list of players appear... assuming there are players on that gameserver.

### Notes
- This code may not scale very well, so server owners should be aware of this before deploying this for large playerbases
- The web interface is very "rough around the edges", as I am not a web designer
- I would recommend using something like PM2 to daemonize this, or even running the code in a detached `tmux` terminal
- Anyone is welcome to suggest new features, and pull requests are greatly appreciated!
