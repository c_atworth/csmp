const express = require('express');
const http = require('http');
const WebSocket = require('ws');
const mongoose = require('mongoose');
const axios = require('axios');
require('dotenv').config()

// THESE ARE DEFINED IN .env!!! DO NOT CHANGE HERE!
const dbun = process.env.DATABASE_USERNAME;
const dbpw = process.env.DATABASE_PASSWORD;
const dbsa = process.env.DATABASE_SERVER_ADDRESS;
const murl = process.env.MAP_SERVER_ADDRESS;
const svpt = process.env.SERVER_PORT;
const scfq = process.env.CONTACT_FREQ;

mongoose.connect(`mongodb://${dbun}:${dbpw}@${dbsa}/playerdb`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


const Player = mongoose.model('Player', {
    uuid: { type: String, required: true, unique: true },
    name: String,
    online: Boolean,
    startTime: Date,
    endTime: Date,
    health: Number,
    armor: Number
});


const app = express();
const server = http.createServer(app);
const wss = new WebSocket.Server({ server });
app.use(express.static('public'));


wss.on('connection', ws => {
	sendPlayerData(ws);
	ws.on('close', () => { });
});


/**
 * A dedicated function to process dispatching data to the websocket client
 * 
 * @param {any} ws - The websocket created for the application
 */
async function sendPlayerData(ws) {
	const players = (await Player.find()).reverse();
	ws.send(JSON.stringify(players));
}


/**
 * Every (n) seconds (defined in .env), contact the map server's API to retreive the current list of players
 */
setInterval(async () => {
	try {
		const response = await axios.get(murl);
		const apiPlayerData = response.data.players;

		await updatePlayers(apiPlayerData);

		wss.clients.forEach(client => {
			if (client.readyState === WebSocket.OPEN) {
				sendPlayerData(client);
			}
		});
	} catch (error) {
		console.log(error);
	}
}, (scfq * 1000));


/**
 * Updates the database with the current players
 * 
 * @param {string} playerData - The data received from the gameserver's map API
 */
async function updatePlayers(playerData) {
	const dbPlayers = await Player.find();

    for (let dbPlayer of dbPlayers) {
        const playerFoundOnMap = playerData.find(player => player.uuid === dbPlayer.uuid); 
        if (!playerFoundOnMap && dbPlayer.online === true) { 
            dbPlayer.online = false;
            dbPlayer.endTime = new Date();
            dbPlayer.startTime = null;
            dbPlayer.health = null;
            dbPlayer.armor = null;
            await dbPlayer.save();
        }
    }

    for (let player of playerData) {
        let dbPlayer = await Player.findOne({ uuid: player.uuid });

        if (!dbPlayer) {
            dbPlayer = new Player({
                uuid: player.uuid, 
                name: player.name,
				online: true,
				startTime: new Date(),
				endTime: null,
                health: player.health,
                armor: player.armor
			});
		} else if (dbPlayer.startTime == null) {
			dbPlayer.online = true;
			dbPlayer.startTime = new Date();
			dbPlayer.endTime = null;
            dbPlayer.health = player.health;
            dbPlayer.armor = player.armor;
		} else if (dbPlayer && dbPlayer.online) {
            if (dbPlayer.health !== player.health || dbPlayer.armor !== player.armor) {
                dbPlayer.health = player.health;
                dbPlayer.armor = player.armor;
                await dbPlayer.save();
            }
        }

		await dbPlayer.save();
	}
}


server.listen(svpt, () => {
	console.log('Server started!');
});